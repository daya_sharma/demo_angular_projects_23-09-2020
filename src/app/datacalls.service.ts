import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';

// import 'rxjs/Rx';
import 'rxjs/add/operator/map';
// import { Observable, throwError } from 'rxjs';
import { Observable} from 'rxjs/observable';
// import { catchError, retry } from 'rxjs/operators';
// import {map} from 'rxjs/operators'
// import { from } from 'rxjs';

/*** 
 * @Injectable({
  providedIn: 'root'
})
OR**/
@Injectable()   // to get http access every where
export class DatacallsService {
  ip = 'http://localhost:3000';
  

  constructor(private _http: Http) { }

  getCategory(id,parent_id) {
    return this._http.get(this.ip + '/getCategory/'+id+'/'+parent_id)
      .map(res => res.json());
  }


}
