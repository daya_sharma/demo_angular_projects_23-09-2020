import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

/** install the below package if not available 
 * > npm install @angular/http@latest 
 * > npm install --save rxjs-compat 
 * > npm update 
 * ***/
import {HttpModule} from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';     
import { MatFormFieldModule, MatSnackBar, MatTableModule } from '@angular/material';
import { ProductsComponent } from './products/products.component';
import { ProductCategoryComponent } from './product-category/product-category.component';
import { AddProductComponent } from './add-product/add-product.component';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { AddCategoryComponent } from './add-category/add-category.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductCategoryComponent,
    AddProductComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatTableModule,
    // FormBuilder,
    // FormGroup,
    // FormControl,
    // FormsModule,
    ReactiveFormsModule,
    
  ],
  providers: [],

  bootstrap: [AppComponent]
})
export class AppModule { }
