import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DatacallsService } from '../datacalls.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers:[DatacallsService]
})
export class ProductsComponent implements OnInit {
  id;
  lotForm;
  post;
  parent_id;

  displayedColumns: string[] = ['id','name','edit','delete'];
  dataSource;
  posts;  

  constructor(private DatacallsService:DatacallsService,private ActivatedRoute:ActivatedRoute,private router:Router) {

   }
  
  
  ngOnInit(){
   
    this.id = this.ActivatedRoute.snapshot.params['id'];
    console.log("patch value id -->", this.id);
    this.parent_id = this.ActivatedRoute.snapshot.params['parent_id'];
    console.log("patch value parent_id -->", this.parent_id);

    if (this.id != 'undefined') {
      this.DatacallsService.getCategory(null,this.id).subscribe(posts => {
        this.posts = posts.result
        console.log('posts---=>',this.posts);
       
        this.dataSource=new MatTableDataSource(this.posts);


      });
    }
    
  }

}