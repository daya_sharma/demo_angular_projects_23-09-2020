import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DatacallsService } from '../datacalls.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css'],
  providers:[DatacallsService]
})
export class AddIndustryComponent implements OnInit {
  id;
  lotForm;
  post;
  posts;
  parent_id;


  constructor(private DatacallsService:DatacallsService,private ActivatedRoute:ActivatedRoute,private FormBuilder:FormBuilder,public snackBar:MatSnackBar,private router:Router) { 

    this.lotForm = this.FormBuilder.group({
      id: [0],
      name: [''], 
    })


  }

  ngOnInit() {

    this.id = this.ActivatedRoute.snapshot.params['id'];
    console.log("patch value id -->", this.id);

  }

  onSubmit(){
    var data={
                "id":this.lotForm.value.id,
                "parent_id":0,
                "name":this.lotForm.value.name
             }
    console.log('this data -=-=-=>',data)
        
      }

}
