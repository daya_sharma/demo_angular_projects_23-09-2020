import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddProductComponent } from './add-product/add-product.component';
import { ProductCategoryComponent } from './product-category/product-category.component';
import { ProductsComponent } from './products/products.component';

const routes: Routes = []=[
   {path:'category',component:ProductCategoryComponent},
  //  {path:'add-category',component:AddProduct},


   {path:'products',component:ProductsComponent},
   {path:'products/:id',component:ProductsComponent},
  {path:'add-products',component:AddProductComponent},
  {path:'edit-products/:id',component:AddProductComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
