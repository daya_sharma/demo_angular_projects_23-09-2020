import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { DatacallsService } from '../datacalls.service';

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.css'],
  providers:[DatacallsService]
})
export class ProductCategoryComponent implements OnInit {
  title = 'demo-app';
  constructor(private DatacallsService:DatacallsService) { }

  displayedColumns: string[] = ['id','name','sub1','edit','delete'];
  dataSource;
  posts;  
  
  
  ngOnInit(){
   
    this.DatacallsService.getCategory(null,0).subscribe(post=>{
      this.posts=post.result;
      console.log('posts data-->>',this.posts);
 
      this.dataSource=new MatTableDataSource(this.posts);
      
    })

    
    
  }

}