import { TestBed } from '@angular/core/testing';

import { DatacallsService } from './datacalls.service';

describe('DatacallsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatacallsService = TestBed.get(DatacallsService);
    expect(service).toBeTruthy();
  });
});
